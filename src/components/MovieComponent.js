import React from 'react';

const Movie = (props) => {
    return (
        <div className="movie">
            <h1>{props.title}</h1>
            <img src={props.img} alt=""/>
            <p>Relized: {props.year}</p>
        </div>
    )
};

export default Movie;