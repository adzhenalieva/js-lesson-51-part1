import React, {Component} from 'react';
import Movie from "./components/MovieComponent";
import Inception from "./images/01.jpg";
import BloodDiamond from "./images/02.jpg";
import Catch from "./images/03.jpg"
import './App.css';

class App extends Component {
  render() {
    return (
        <div className="main">
          <Movie title="Catch Me If You Can" img={Catch} year="2002"/>
          <Movie title="Blood diamond" img={BloodDiamond} year="2006"/>
          <Movie title="Inception" img={Inception} year="2010"/>
        </div>
    );
  }
}


export default App;
